const ethers = require('ethers');

// Chose the length of your mnemonic:
//   - 16 bytes => 12 words (* this example)
//   - 20 bytes => 15 words
//   - 24 bytes => 18 words
//   - 28 bytes => 21 words
//   - 32 bytes => 24 words

// Select the language:
//   - en, es, fr, ja, ko, it, zh_ch, zh_tw
let language = ethers.wordlists.en;

// Set the number of mnemonics you want to print 
//  - 50 => 50 unique mnemonics 
let count = 1; 

// call this function to generate mnemonic 
function generate(count) {
  let bytes = ethers.utils.randomBytes(16);
  let randomMnemonic = ethers.utils.entropyToMnemonic(bytes, language);
  console.log("YOUR-MESSAGE-HERE:", Math.random().toString(36).slice(2), "\n", randomMnemonic, "\n");
}

// run generate in loop 'count' number of times 
for (i = 0; i < count; i++) {
  generate();
} 
